package it.pegaso.compito.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class IstanzaDiTest implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "testId")
	private Test test;
	
	@ManyToOne
	@JoinColumn(name = "utenteId")
	private Utente utente;
	
	private Date date;
	
	private int punteggio;
	
	@OneToMany(mappedBy = "istanzaDiTest")
	private List<IstanzadiTestRisposte> risposteList;
	
	
	
	

}
