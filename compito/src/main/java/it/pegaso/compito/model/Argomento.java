package it.pegaso.compito.model;

public enum Argomento {
	
	JAVA("JAVA"), ANGULAR("ANGULAR"), C("C");
	
private String description;
	
	public String getDescription() {
		return description;
	}

	private Argomento(String description) {
		this.description = description;
	}

}
