package it.pegaso.compito.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class IstanzadiTestRisposte implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@ManyToOne
	@JoinColumn(name  = "istanzaDiTestId")
	private IstanzaDiTest istanzaDiTest;
	
	@ManyToOne
	@JoinColumn(name  = "domandaId")
	private Domanda domanda;
	
	@OneToOne
	@JoinColumn(name  = "opzioneDomandaId")
	private OpzioneDomanda opzioneDomanda;

}
