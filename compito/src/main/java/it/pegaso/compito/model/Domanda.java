package it.pegaso.compito.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode
public class Domanda implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String testo;
	
	private Argomento argomento;
	
	@OneToMany(mappedBy = "domanda")
	private List<OpzioneDomanda> opzioniList;
	
	@ManyToOne
	@JoinColumn(name = "pool_domande")
	private PoolDomande pool;
	

}
