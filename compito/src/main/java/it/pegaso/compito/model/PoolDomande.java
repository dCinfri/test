package it.pegaso.compito.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class PoolDomande {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private Argomento argomento;
	
	@OneToMany(mappedBy = "pool")
	private List<Test> testList;
	
	@OneToMany(mappedBy = "pool")
	private List<Domanda> domandeList;
	
	

}
